name	"boombond_cookbook"

depends "scm_helper"
depends "opsworks_initial_setup"
depends "opsworks_commons"
depends "opsworks_berkshelf"

recipe "boombond_cookbook::install_lame", "Install lame encoder via aptitude"
recipe "boombond_cookbook::install_image_magic", "Install image magic via aptitude"
recipe "boombond_cookbook::delayed_job", "Run delayed_job with: rake jobs:work"
recipe "boombond_cookbook::folder_permissions", "Modify folder permission"


 