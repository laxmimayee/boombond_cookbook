node[:deploy].each do |application, deploy|
 Chef::Log.info("application path: #{deploy[:deploy_to]}");
 script "delayed_job" do
   interpreter "bash"
   user "root"
   cwd "#{deploy[:deploy_to]}/current"
   code <<-EOH
    RAILS_ENV=production bin/delayed_job start
   EOH
 end
end

