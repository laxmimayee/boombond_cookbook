node[:deploy].each do |application, deploy|
 directory "#{deploy[:deploy_to]}/shared"  do
   mode '0775'
 end
 directory "#{deploy[:deploy_to]}/shared/system"  do
   mode '0775'
 end
end
